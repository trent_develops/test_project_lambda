# Test Python Lambda
[![pipeline status](https://gitlab.com/trent_develops/test_project_lambda/badges/master/pipeline.svg)](https://gitlab.com/trent_develops/test_project_lambda/commits/master)[![coverage report](https://gitlab.com/trent_develops/test_project_lambda/badges/master/coverage.svg)](https://gitlab.com/trent_develops/test_project_lambda/commits/master)

## Next steps:
- Configure EKS to use Auto-Devops? Costs $$$
- aws chalice?
- review apps (deploy resources once)
- coverage reports
- add lambda capabilities